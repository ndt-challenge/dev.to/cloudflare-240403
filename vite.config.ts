import { defineConfig } from 'vite'
import SolidPlugin from 'vite-plugin-solid'

export default defineConfig({
   plugins: [
      SolidPlugin(),
   ],
   server: {
      port: 3000,
   },
   build: {
      target: 'esnext',
      outDir: 'dist'
   },
})