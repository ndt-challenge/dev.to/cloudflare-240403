const headers = {
   "Content-Type": "text/event-stream",
   // 'Access-Control-Allow-Origin': 'https://cloudflare-ai-challenge.pages.dev',
   'Access-Control-Max-Age': '86400',
}

export const bots: Record<string, Bot> = {
   'johanna': {
      type: '@cf/meta/llama-2-7b-chat-int8',
      context: 'Your name is Johanna, and you are an IT expert who speaks English.',
   },
   'rosa': {
      type: '@cf/meta/llama-2-7b-chat-fp16',
      context: 'Your name is Rosa, and you are a physiscian who speaks English.',
   },
   'felicita': {
      type: '@cf/mistral/mistral-7b-instruct-v0.1',
      context: 'Your name is Felicita, and you are a farmer who speaks English.',
   },
   'hira': {
      type: '@hf/thebloke/llama-2-13b-chat-awq',
      context: 'Your name is Hira, and you are a commedian who speaks English. Your words always are humorous and grateful.',
   },
   'makbule': {
      type: '@cf/tinyllama/tinyllama-1.1b-chat-v1.0',
      context: 'Your name is Makbule, and you are a politician of Viet Lam who speaks English.',
   },
   'nefertari': {
      type: '@cf/qwen/qwen1.5-7b-chat-awq',
      context: 'Your name is Nefertari, and you are a baddass soldier who speaks English.',
   },
}

function getmsg(prompt: string, context?: string)
{
   return [
      { role: 'system', content: context },
      { role: 'user', content: prompt }
   ]
}

export async function onRequestGet(ctx: EventContext<Env, any, Record<string, unknown>>): Promise<Response>
{
   const { type, context } = bots[ctx.params.name as string]
   const prompt = new URL(ctx.request.url).searchParams.get('prompt') as string
   const input = {
      messages: getmsg(prompt, context),
      stream: true
   }

   const stream = await ctx.env.AI.run(type, input)
   return new Response(stream, { headers })
}
