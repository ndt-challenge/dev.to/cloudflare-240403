# Cloudflare AI Challenge 

(**03-April-2024** to **14-April-2024**) |
[Article](https://dev.to/devteam/join-us-for-the-cloudflare-ai-challenge-3000-in-prizes-5f99) | 
[Info](https://dev.to/challenges/cloudflare)

😞 I missed this challenge, but I still wanna do it so here is my toy code 😞

![](./thumbnail.png)

## The Prompt 

> For this challenge, you will be deploying a serverless AI app on Cloudflare workers. Your mandate is to build a Workers AI application that makes use of AI task types from Cloudflare’s growing catalog of Open Models.
> 
> Cloudflare’s development platform offers a plethora of Open Models and the ability to accomplish many tasks related to text, images, and audio. Here is just a small taste of what you can do:
>
> - Text Generation (10+ Open Models to choose from!)
> - Object Detection
> - Speech Recognition
> - Translation
> - And More!

### TODO

1. user enters 1 input and observer how it being passed to other bot
   - user pass input to bot A
   - bot A generates result A
   - bot A pass input to bot B
   - bot B generates result B
   - bot B pass input to bot C
   - bot C generates result C
   - **»** observe if bots can actually talk to each other
2. try to display word by word in bot chat like ChatGPT with SolidJs + WorkerAI stream mode

## Reference

- https://developers.cloudflare.com/workers-ai/get-started

## Local Development

Require:

- node
- pnpm
- wrangler

**Recommend** `volta` manager for all 3 tools above.

```shell
# Shell 1 (cloudflare dev mode)
pnpm preview

# Shell 2 (build frontend)
pnpm dev
```

### Troubleshooting

- `wrangler` exit **code 7**  
   ```shell
   # kill all servers (yes, it could go up multiple ones)
   killall -9 workerd
   # start the server again
   pnpm preview
   ```
- Where do I get more AI models?
   - https://developers.cloudflare.com/workers-ai/models

## Credit

- `/public/favicon.ico` (production) : https://dev.to
- `/favicon.ico` (development) : https://yuru-camp.fandom.com
- All bots' avatar: https://www.dicebear.com/styles/bottts

