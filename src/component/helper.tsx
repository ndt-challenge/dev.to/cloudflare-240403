import { createContext, useContext, type Signal } from 'solid-js'
import cls from './helper.module.css'

export function Empty()
{
   return <p class={cls['on-empty']}>Nothing here yet...<br />Try to type on something to ignite the chat, shall we?</p>
}

export const Context = createContext<Signal<string>>()
export function use_idea()
{
   return useContext(Context)
}