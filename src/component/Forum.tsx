import { For, createEffect, createSignal } from 'solid-js'
import { listen_bot, shuffle_botlist } from '../utillity'

import Message, { type MessageProps } from './Message'
import { Empty, use_idea } from './helper'

import cls from './Forum.module.css'


export default function Forum()
{
   const botlist = shuffle_botlist()

   const [idea] = use_idea()!
   const [conn, set_conn] = createSignal(2)
   const [list, set_list] = createSignal<MessageProps[]>([])

   console.log(conn)

   createEffect(() =>
   {
      if (!idea()) return void 0

      console.log('before check', conn(), botlist)
      // stop adding more if it is connecting to other bot
      if (conn() === 1) return void 0
      console.log('after check', botlist)

      const name = botlist.pop()
      if (!name) return void 0
      const content = listen_bot(name, idea(), [conn, set_conn])
      set_list([...list(), { name, content }])
      console.log('on listen', conn(), botlist)
   })

   return (
      <figure class={cls.forum}>
         <For each={list()} fallback={<Empty />}>
            {m => <Message name={m.name} content={m.content} />}
         </For>
         <button class={cls.more} onClick={_ => 0}>Continue</button>
      </figure>
   )
}