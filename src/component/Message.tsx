import { type Accessor } from 'solid-js'

import cls from './Message.module.css'

export interface MessageProps
{
   name: string
   content: Accessor<string>
}

export default function Message(props: MessageProps)
{
   console.log(props.content())

   return (
      <blockquote class={cls.message}>
         <header class={cls.bot}>
            <img class={cls.avatar} src={`/avatar/${props.name.toLowerCase()}.svg`} alt='' />
            <strong class={cls.name}>{props.name}</strong>
         </header>
         <p class={cls.content}>{props.content()}</p>
      </blockquote>
   )
}