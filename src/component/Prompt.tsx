import cls from './Prompt.module.css'

import { use_idea } from './helper'


export default function Prompt()
{
   let input: HTMLInputElement | undefined
   const [idea, set_idea] = use_idea()!

   const callbot = async (e: SubmitEvent) =>
   {
      e.preventDefault()
      try
      {
         input?.value && set_idea(input.value)
      }
      catch (e)
      {
         console.error(e)
      }
      finally
      {
         input && input.value && (input.disabled = true)
      }
   }

   return (
      <form class={cls.prompt} onSubmit={callbot}>
         <input
            type='text'
            class={cls.input}
            value={idea()}
            onChange={e => set_idea(e.target.value)}
            placeholder='Any funny idea?'
            ref={input}
         />
         <input
            type='submit'
            class={cls.submit}
            value='go'
         />
      </form>
   )
}