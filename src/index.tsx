/* @refresh reload */
import { render } from 'solid-js/web'

import './global.css'
import { createContext, createSignal } from 'solid-js'

import Prompt from './component/Prompt'
import Forum from './component/Forum'
import { Context } from './component/helper'


function App()
{
   const idea = createSignal('')

   return (
      <Context.Provider value={idea}>
         <Prompt />
         <header data-ico='⚠️'>
            Machines are stupid, please use this for entertainment only 😅
         </header>
         <Forum />
      </Context.Provider>
   )
}

render(
   () => <App />,
   document.querySelector('main')!
)