import { createSignal, type Accessor, type Signal } from 'solid-js'

// const botlist = ['Johanna', 'Rosa', 'Felicita', 'Hira', 'Makbule', 'Nefertari']
const botlist = ['Johanna']

export function randbot(): string
{
   const max = botlist.length - 1
   const min = 0
   const inx = Math.floor(Math.random() * (max - min) + min)

   return botlist[inx]
}

// Source:
// https://stackoverflow.com/questions/2450954
export function shuffle_botlist()
{
   const ls = [...botlist]
   let curinx = ls.length

   // While there remain elements to shuffle...
   while (curinx != 0)
   {
      // Pick a remaining element...
      let randinx = Math.floor(Math.random() * curinx)
      curinx--;

      // And swap it with the current element.
      [ls[curinx], ls[randinx]] = [ls[randinx], ls[curinx]]
   }

   return ls
}

export function listen_bot(name: string, input: string, conn: Signal<number>)
{
   const [_, set_state] = conn
   const [buf, set_buf] = createSignal('')

   const source = new EventSource(`bot/${name.toLowerCase()}?prompt=${input}`)

   source.addEventListener('open', _ => set_state(source.readyState))
   source.addEventListener('message', e =>
   {
      if (e.data === '[DONE]')
      {
         source.close()
         return void set_state(source.readyState)
      }

      const { response } = JSON.parse(e.data) as AiResponse
      console.log('before set_buf', buf())
      set_buf(buf() + response)
      console.log('after set_buf', buf())
   })

   return buf
}