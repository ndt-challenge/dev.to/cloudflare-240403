interface Env
{
   AI: any
}

interface AiResponse
{
   response: string
}

interface Bot
{
   type: `@${'cf' | 'hf'}/${string}/${string}`,
   context: string,
}

