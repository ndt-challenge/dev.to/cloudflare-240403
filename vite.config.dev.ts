import { defineConfig } from 'vite'
import SolidPlugin from 'vite-plugin-solid'

// For more info see https://github.com/thetarnav/solid-devtools/tree/main/packages/extension#readme
import devtools from 'solid-devtools/vite'

export default defineConfig({
   plugins: [
      devtools(),
      SolidPlugin(),
   ],
   server: {
      port: 3000,
   },
   build: {
      target: 'esnext',
      outDir: 'dist'
   },
})